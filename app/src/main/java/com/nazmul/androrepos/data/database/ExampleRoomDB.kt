package com.nazmul.androrepos.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.nazmul.androrepos.data.database.daos.GitHubRepoDAO
import com.nazmul.androrepos.data.model.GitHubRepoModel
import com.nazmul.androrepos.utils.C
import com.nazmul.androrepos.data.model.GitHubRepoResponse
import com.nazmul.androrepos.data.model.RepoOwnerModel

/**
 * Room Database class. Refer to README for more information
 */
@Database(entities = [GitHubRepoModel::class, RepoOwnerModel::class], version = 12, exportSchema = false)
abstract class ExampleRoomDB : RoomDatabase() {

    abstract fun getGithubRepoDao(): GitHubRepoDAO

    /**
     *  Creates a way to ensure that the database accessed in different locations is the same
     *  instance. Also known as a Singleton pattern. Further explained in the Employee Repository.
     */
    companion object {
        private var INSTANCE: ExampleRoomDB? = null

        fun getDatabase(context: Context) = INSTANCE ?: kotlin.run {
            Room.databaseBuilder(
                context.applicationContext,
                ExampleRoomDB::class.java,
                C.ROOM_DB_NAME
            )
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}
