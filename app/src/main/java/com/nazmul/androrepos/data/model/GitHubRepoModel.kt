package com.nazmul.androrepos.data.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

// 06 This creates our class, and defines our constructor, all in one line
@Entity(tableName = "github_repo_table")
data class GitHubRepoModel(

    @SerializedName("id")
    @Expose
    @PrimaryKey
    val id: Int,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("full_name")
    @Expose
    val fullName: String,
    @SerializedName("stargazers_count")
    @Expose
    val starCount: Int,
    @SerializedName("watchers_count")
    @Expose
    val watchCount: Int,
    @SerializedName("node_id")
    @Expose
    val profileImage: String,
    @SerializedName("description")
    @Expose
    val description: String?,
    @SerializedName("updated_at")
    @Expose
    val updatedAt: String,
    @SerializedName("created_at")
    @Expose
    val createdAt: String,
    var updatedAtMills: Long,
    var ownerId: Int,
    @SerializedName("owner")
    @Ignore val owner: RepoOwnerModel?
) {
    constructor(
        id: Int,
        name: String,
        fullName: String,
        starCount: Int,
        watchCount: Int,
        profileImage: String,
        description: String?,
        updatedAt: String,
        createdAt: String,
        updatedAtMills: Long,
        ownerId: Int
    ) : this(id, name, fullName, starCount, watchCount, profileImage, description, updatedAt, createdAt, updatedAtMills, ownerId, null)
}

