package com.nazmul.androrepos.data.api

import com.nazmul.androrepos.data.model.GitHubRepoResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiEndpoints {

    // Simple get request to get a list of

    @GET("search/repositories")
    fun getGitHubRepoList(@Header("Authorization") token: String,
                          @Query("q") topic: String,
                          @Query("sort") sortBy: String,
                          @Query("order") order: String,
                          @Query("per_page") parPage: Int,
                          @Query("page") pageNo: Int
    ): Call<GitHubRepoResponse> //Call<ResponseBody>//

}
