package com.nazmul.androrepos.data.api

import com.nazmul.androrepos.utils.C
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetroFitInstance {

    private var INSTANCE: Retrofit? = null

    fun getInstance(): Retrofit = INSTANCE ?: kotlin.run {
        Retrofit.Builder()
            .baseUrl(C.SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}
