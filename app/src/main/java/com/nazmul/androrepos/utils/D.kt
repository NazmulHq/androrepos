package com.nazmul.androrepos.utils
import android.content.Context
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.nazmul.androrepos.R

/**
 * Created by Nazmul Haque.
 * bismillah :)
 */
object D {
    private val TAG = D.javaClass.simpleName
    private var noNetDialog: AlertDialog? = null

    fun showNoInternetDialog(context: Context, retryNoNet: Function0<Unit>?) {
        if (noNetDialog != null && noNetDialog!!.isShowing)
            return
        try {
            val b = AlertDialog.Builder(context, R.style.AlertDialogStyle)
            b.setMessage("No internet! Please connect to internet ...")
            b.setNegativeButton("Settings") { dialog, _ ->
                dialog.dismiss()
                //val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                //context.startActivity(intent)
            }
            if (retryNoNet != null)
                b.setPositiveButton("Retry") { dialog, _ ->
                    dialog.dismiss()
                    retryNoNet.invoke()
                }
            noNetDialog = b.create()
            //setDialogThemes(noNetDialog)
            noNetDialog!!.show()
        } catch (e: Exception) {
            Log.e(TAG, "Exception during showing the No-Internet-Dialog.", e)
        }
    }

    fun showSnackBar(view: View, msg: String){
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()
    }
}