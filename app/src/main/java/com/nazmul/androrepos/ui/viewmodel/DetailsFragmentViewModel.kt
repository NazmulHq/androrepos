package com.nazmul.androrepos.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.nazmul.androrepos.data.model.GitHubRepoModel
import com.nazmul.androrepos.data.model.RepoOwnerModel
import com.nazmul.androrepos.data.repository.GitHubRepRepository

class DetailsFragmentViewModel(application: Application) : AndroidViewModel(application) {

    // coordinates between the local and remote databases
    private val gitHubRepository = GitHubRepRepository.getInstance(application)

    // This LiveData is created using a ktx library shortcut
    fun gitHubRepoOwnerLiveData(ownerId: Int): LiveData<RepoOwnerModel?>  {
        return gitHubRepository.getRepoOwnerData(ownerId)
    }

    fun gitHubRepoLiveData(repoId: Int): LiveData<GitHubRepoModel?>  {
        return gitHubRepository.getRepoData(repoId)
    }
}
