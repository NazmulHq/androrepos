package com.nazmul.androrepos.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.nazmul.androrepos.data.model.GitHubRepoModel
import com.nazmul.androrepos.data.model.GitHubRepoResponse
import com.nazmul.androrepos.data.repository.GitHubRepRepository
import kotlinx.coroutines.Dispatchers

class ListFragmentViewModel(application: Application) : AndroidViewModel(application) {

    // coordinates between the local and remote databases
    private val gitHubRepository = GitHubRepRepository.getInstance(application)

    // This LiveData is created using a ktx library shortcut
    val gitHubRepoListLiveData: LiveData<List<GitHubRepoModel>> = liveData(Dispatchers.IO) {
        emitSource(gitHubRepository.getAllGitHubRepoLiveData())
    }
}
