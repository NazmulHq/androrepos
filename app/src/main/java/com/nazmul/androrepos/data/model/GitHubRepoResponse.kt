package com.nazmul.androrepos.data.model

import com.google.gson.annotations.SerializedName

data class GitHubRepoResponse(
    @SerializedName("total_count")
    val totalCount: Int,

    @SerializedName("incomplete_results")
    val incompleteResult: Boolean,

    @SerializedName("items")
    val items: List<GitHubRepoModel>
)