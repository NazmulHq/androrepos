package com.nazmul.androrepos.utils


/**
 * Created by Nazmul Haque.
 * bismillah :)
 */

object C {

    const val IS_APP_DEBUGGABLE = true

    @JvmField
    var SERVER_URL: String = "https://api.github.com" //"http://dummy.restapiexample.com/"
    const val APP_NAME = "androrepos "

    const val ROOM_DB_NAME = "ROOM_DB_GIT_REPOSITORY"

    const val TOKEN = "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"

    const val GITHUB_REPO_OWNER_ID = "OWNER_ID"
    const val GITHUB_REPO_ID = "REPO_ID"

    const val SORT_BY = "sort_by"
    const val BY_STAR = "stars"
    const val BY_UPDATE ="updated"

    const val ORDER_BY = "desc"
    const val PER_PAGE = 10

    const val SEARCH_TOPIC = "android"


    const val NET_TIMEOUT_READ = 80L
    const val NET_TIMEOUT_WRITE = 120L
    const val NET_TIMEOUT_CONNECT = 75L

    object HTTP {
        const val OK = 200
        const val OK_MAX = 299
        const val INVALID = 400
        const val  UNAUTHORIZED = 401
        const val FORBIDDEN = 403
        const val NOT_FOUND = 404
        const val NOT_ACCEPTABLE = 406
        const val TIMEOUT = 408
        const val INVALID_MAX = 499
        const val ERROR = 500
        const val ERROR_MAX = 599
        const val GATEWAY_TIMEOUT = 504
    }

}
