package com.nazmul.androrepos.data.database.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nazmul.androrepos.data.model.GitHubRepoModel
import com.nazmul.androrepos.data.model.RepoOwnerModel

@Dao
interface GitHubRepoDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGitHubRepo(gitHubRepoModel: GitHubRepoModel)

    @Delete
    fun deleteGitHubRepo(gitHubRepoModel: GitHubRepoModel)

    @Query("SELECT * FROM github_repo_table")
    fun getAllGitHubReposLiveData(): LiveData<List<GitHubRepoModel>>

    @Query("SELECT * FROM github_repo_table ORDER BY updatedAtMills DESC")
    fun getAllGitHubReposLiveDataOrderByUpdate(): LiveData<List<GitHubRepoModel>>

    @Query("SELECT * FROM github_repo_table ORDER BY starCount DESC")
    fun getAllGitHubReposLiveDataOrderByStarCount(): LiveData<List<GitHubRepoModel>>

    @Query("SELECT * FROM github_repo_table WHERE id = :repoId")
    fun getGitHubRepoByIdLiveData(repoId: Int): LiveData<GitHubRepoModel?>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepoOwner(gitHubRepoModel: RepoOwnerModel)

    @Query("SELECT * FROM repo_owner_table WHERE id = :ownerId")
    fun getRepoOwnerData(ownerId: Int): LiveData<RepoOwnerModel?>
}
