# Anro Repo

### Main Tasks
1. Fetch repository list from GitHub API using "Android" as query keyword. (API doc). (Done)
2. The fetched data should be stored in a local database to permit the app to be used offline
   mode. (Done)
3. Fetching the repository list should be paginated by scrolling. Each time by scrolling, fetch 10
   new items. (Not completed)
4. The required data can be refreshed from the API no more frequently than once every 30
   minutes. (Not completed)
5. Show the list of repositories in the home page. (Done)
6. List can be sorted by either last updated date-time or star count (add a sorting button/icon) (Not completed)
7. Selected sorting option persists in further app sessions. (Done)
8. A repo details page, to which navigated by clicking on an item from the list. (Done)
9. Details page shows repo owner's name, photo, repository's description, last update date time
   in month-day-year hour:seconds format, each field in 2 digit numbers and any other fields
   you want. (Done)
10. The repository list and repository details data which loaded once, should be saved for offline
    browsing. (Done)

### Screenshots
<img src="https://gitlab.com/NazmulHq/androrepos/-/raw/master/screenshot/HomeListFragment.png?raw=true" width="222">
<img src="https://gitlab.com/NazmulHq/androrepos/-/raw/master/screenshot/DetailsFragment.png?raw=true" width="222">
<img src="https://gitlab.com/NazmulHq/androrepos/-/raw/master/screenshot/SortOption.png" width="222">

### Author
Nazmul Haque