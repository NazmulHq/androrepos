package com.nazmul.androrepos.data.repository

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.nazmul.androrepos.data.api.ApiEndpoints
import com.nazmul.androrepos.data.api.RetroFitInstance
import com.nazmul.androrepos.data.database.ExampleRoomDB
import com.nazmul.androrepos.data.database.daos.GitHubRepoDAO
import com.nazmul.androrepos.data.model.GitHubRepoModel
import com.nazmul.androrepos.data.model.GitHubRepoResponse
import com.nazmul.androrepos.data.model.RepoOwnerModel
import com.nazmul.androrepos.utils.C
import com.nazmul.androrepos.utils.U
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

/**
 * Repository classes coordinate between local and remote databases. This provides what is known as
 * a 'single source of truth'.
 */

/**
 * 'private constructor' prevents this class from being instatiated directly. A builder function
 *  is provided in a companion object instead. This is essentially a Singleton pattern from java.
 */
class GitHubRepRepository private constructor(application: Application) {

    private val gitHubRepoDAO: GitHubRepoDAO = ExampleRoomDB.getDatabase(application).getGithubRepoDao()
    private val apiCalls = RetroFitInstance.getInstance().create(ApiEndpoints::class.java)

    /**
     * An 'init' block in Kotlin is called immediately after an Object is created, and never again.
     * This block is used only for demoing the Singleton pattern. EmployeeRepository is accessed
     * from many classes at different times. If you watch the Logcot messages while switching
     * between screens, and filter by this class name, you'll notice that this message only appears
     * once, implying that its constructor is only being called once.
     */
    init {
        Timber.d("${this.javaClass.name} init for the first time.")
    }

    private suspend fun insertGitHubRepo(gitHubRepoModel: GitHubRepoModel) {
        gitHubRepoDAO.insertGitHubRepo(gitHubRepoModel)
    }

    private suspend fun insertRepoOwner(repoOwnerModel: RepoOwnerModel) {
      val status =  gitHubRepoDAO.insertRepoOwner(repoOwnerModel)
      Log.d(TAG, "insert owner status: $status")
    }

    private suspend fun deleteGitHubRepo(gitHubRepoModel: GitHubRepoModel) {
        gitHubRepoDAO.deleteGitHubRepo(gitHubRepoModel)
    }

    fun getAllGitHubRepoLiveData(): LiveData<List<GitHubRepoModel>> {
        Log.d("GitHubRepoRepository", "")
        var sortBy = ""
        if(context!=null)
        sortBy = U.getSharedPrefStringVal(context!!.applicationContext, C.SORT_BY).toString()

        if(sortBy == C.BY_UPDATE) {
            return gitHubRepoDAO.getAllGitHubReposLiveDataOrderByUpdate().also {
                getAllGitHubReposFromRemote(1)
            }
        } else if(sortBy == C.BY_STAR){
            return gitHubRepoDAO.getAllGitHubReposLiveDataOrderByStarCount().also {
                getAllGitHubReposFromRemote(1)
            }
        } else {
            return gitHubRepoDAO.getAllGitHubReposLiveData().also {
                getAllGitHubReposFromRemote(1)
            }
        }
    }

    fun getRepoOwnerData(ownerId: Int): LiveData<RepoOwnerModel?> {
        Log.d("GitHubRepoRepository", "")
        return gitHubRepoDAO.getRepoOwnerData(ownerId)
    }

    fun getRepoData(ownerId: Int): LiveData<GitHubRepoModel?> {
        Log.d("GitHubRepoRepository", "")
        return gitHubRepoDAO.getGitHubRepoByIdLiveData(ownerId)
    }

    private fun getAllGitHubReposFromRemote(pageNo: Int) {
        Log.d("GitHubRepository", "e: LiveData()")
        var sortBy = ""
        if(context!=null)
            sortBy = U.getSharedPrefStringVal(context!!, C.SORT_BY).toString()
        try {
            apiCalls.getGitHubRepoList(C.TOKEN, C.SEARCH_TOPIC, sortBy,  C.ORDER_BY, C.PER_PAGE, pageNo).enqueue(object :
                Callback<GitHubRepoResponse> {
                override fun onResponse(
                    call: Call<GitHubRepoResponse>,
                    response: Response<GitHubRepoResponse>
                ) {
                    val respCode = response.code()
                    val resp =
                        if (respCode >= C.HTTP.OK && respCode <= C.HTTP.OK_MAX) {
                            response.body()
                        }
                        else {
                            response.errorBody()
                        }

                    val body  = response.body()
                    val gitHubRepoList = body?.items

                    gitHubRepoList?.forEach {

                        Log.d("Repository name", it.name)
                        GlobalScope.launch(Dispatchers.IO) {
                            val repo = it
                            val owner = repo.owner
                            if (owner != null) {
                                Log.d("Repository owner name", owner.login)
                                repo.ownerId = owner.id
                                repo.updatedAtMills =  U.getTimeStamp(repo.updatedAt)
                                insertGitHubRepo(repo)
                                insertRepoOwner(owner)
                            }
                        }
                    }

                    Log.d("getApiResp", " code: $respCode size: ${gitHubRepoList?.size}")
                }

                override fun onFailure(call: Call<GitHubRepoResponse>, t: Throwable) {
                   // callBack.onError(" error: ${t.message}")
                    Log.d("getAllGitHubRepo", "enqueue error: ${t.message}")
                }
            })

        } catch (exception: Throwable) {
            Timber.e(exception)
        }
    }

    // Singleton Pattern for Repository.
    companion object {

        private val TAG = GitHubRepRepository::class.java.simpleName
        /**
         *  This is where the EmployeeRepository all callers will receive. Set it to null at first
         *  and make it private so it can't be directly accessed.
        */
        private var INSTANCE: GitHubRepRepository? = null
        private var context: Context? =null

        /**
         * This method checks whether or not INSTANCE is null. If it's not null, it returns the
         * Singleton INSTANCE. If it is null, it creates a new Object, sets INSTANCE equal to that,
         * and returns INSTANCE. From here on out, this method will now return the same INSTANCE,
         * every time.
         */
        fun getInstance(application: Application): GitHubRepRepository = INSTANCE ?: kotlin.run {
            context = application.applicationContext
            INSTANCE = GitHubRepRepository(application = application)
            INSTANCE!!

        }
    }
}
