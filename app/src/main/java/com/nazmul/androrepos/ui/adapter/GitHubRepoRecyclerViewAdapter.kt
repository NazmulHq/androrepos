package com.nazmul.androrepos.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nazmul.androrepos.R
import com.nazmul.androrepos.data.model.GitHubRepoModel
import com.nazmul.androrepos.utils.U

/**
 * Uses a ListAdapter<DataClass, ViewHolder> instead of a standard RecyclerViewAdapter. This class
 * offers built in animations when updating a data set, like in a LiveData Observer.
 *
 */
class GitHubRepoRecyclerViewAdapter(private val onClickListener: (GitHubRepoModel) -> Unit) :
    ListAdapter<GitHubRepoModel, GitHubRepoViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitHubRepoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_github_respo, parent, false)
        return GitHubRepoViewHolder(view, onClickListener)
    }

    override fun onBindViewHolder(holder: GitHubRepoViewHolder, position: Int) {
        holder.item = getItem(position)
    }
}

class GitHubRepoViewHolder(private val view: View, private val onClickListener: (GitHubRepoModel) -> Unit) :
    RecyclerView.ViewHolder(view) {

    var item: GitHubRepoModel? = null
        set(value) {
            value?.let { newValue ->
                field = newValue
                view.setOnClickListener { onClickListener(newValue) }
                view.findViewById<TextView>(R.id.textViewName).text = newValue.fullName
                view.findViewById<TextView>(R.id.textViewId).text = "Updated at: ${U.getFormattedTime(newValue.updatedAt)}"
                view.findViewById<TextView>(R.id.list_item_star_count_tv).text = "${newValue.starCount}"
            }
        }
}

class DiffCallback : DiffUtil.ItemCallback<GitHubRepoModel>() {

    override fun areItemsTheSame(oldItem: GitHubRepoModel, newItem: GitHubRepoModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: GitHubRepoModel, newItem: GitHubRepoModel): Boolean {
        return oldItem == newItem
    }
}
