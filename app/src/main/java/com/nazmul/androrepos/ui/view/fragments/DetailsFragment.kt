package com.nazmul.androrepos.ui.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.nazmul.androrepos.R
import com.nazmul.androrepos.databinding.FragmentDetailsBinding

import com.nazmul.androrepos.ui.viewmodel.DetailsFragmentViewModel
import com.nazmul.androrepos.utils.C
import com.nazmul.androrepos.utils.U


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class DetailsFragment : Fragment(R.layout.fragment_details) {

    private var _binding: FragmentDetailsBinding? = null

    private val viewModel by viewModels<DetailsFragmentViewModel>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var repoId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)

        val repoId = arguments?.getInt(C.GITHUB_REPO_ID, -1)
        val ownerId = arguments?.getInt(C.GITHUB_REPO_OWNER_ID, -1)

        Log.d("Details Fragment", "ownerId: $ownerId")
        if (ownerId != null) {
            viewModel.gitHubRepoOwnerLiveData(ownerId).observe(viewLifecycleOwner) { owner ->
                Log.d("Details Fragment", "owner url: ${owner?.url}")

                U.setDirectImage(requireContext(),
                    owner!!.avatarUrl,
                    binding.detailsFragmentOwnerAvatarIv,
                    com.google.android.material.R.drawable.ic_mtrl_chip_close_circle)

                binding.detailsFragmentOwnerNameTv.text = "User id: ${owner.login}"
                binding.detailsFragmentTypeTv.text = "Account type: ${owner.type}"
            }
        }

        if(repoId != null){
            viewModel.gitHubRepoLiveData(repoId).observe(viewLifecycleOwner) { repo ->
                Log.d("Details Fragment", "repo: ${repo?.ownerId}")
                binding.detailsFragmentRepoDescriptionTv.text = "Description: ${repo?.description}"
                binding.detailsFragmentLastUpdateTv.text = "Last update at: ${U.getFormattedTime(repo?.updatedAt)}"
            }
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*binding.buttonSecond.setOnClickListener {
            findNavController().navigate(R.id.action_details_fragment_to_list_fragment)
        }*/
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}