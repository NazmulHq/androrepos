package com.nazmul.androrepos.ui.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.core.os.bundleOf
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.nazmul.androrepos.R
import com.nazmul.androrepos.databinding.FragmentListBinding
import com.nazmul.androrepos.ui.adapter.GitHubRepoRecyclerViewAdapter
import com.nazmul.androrepos.ui.viewmodel.ListFragmentViewModel
import com.nazmul.androrepos.utils.C
import com.nazmul.androrepos.utils.C.GITHUB_REPO_OWNER_ID
import com.nazmul.androrepos.utils.C.GITHUB_REPO_ID
import com.nazmul.androrepos.utils.D
import com.nazmul.androrepos.utils.U

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ListFragment : Fragment(R.layout.fragment_list), MenuProvider{

    private var TAG = "ListFragment"
    private var _binding: FragmentListBinding? = null

    private val viewModel by viewModels<ListFragmentViewModel>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()

        if (!U.isNetConnected(context)) {
            D.showSnackBar(view, "No internet connection")
        }
        getData(false)

        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }
    private fun initRecycler() {
        binding.recyclerView.apply {
            this.layoutManager = LinearLayoutManager(this.context)
            this.adapter = GitHubRepoRecyclerViewAdapter { gitHubRepo ->
                val bundle = bundleOf(
                    GITHUB_REPO_OWNER_ID to gitHubRepo.ownerId,
                    GITHUB_REPO_ID to gitHubRepo.id
                )
                findNavController().navigate(R.id.details_fragment, bundle)
            }

        }
    }

    private fun getData(isNotifyDataSetChanged: Boolean){
        val adapter = (binding.recyclerView.adapter as GitHubRepoRecyclerViewAdapter)
        if(isNotifyDataSetChanged){
            adapter.submitList(null)
            viewModel.gitHubRepoListLiveData.removeObservers(viewLifecycleOwner)

        }
        viewModel.gitHubRepoListLiveData.observe(viewLifecycleOwner) { repositoryList ->

            Log.d("ListFragment", "data got size : ${repositoryList.size}")

            binding.cardViewListLoading.visibility = when {
                repositoryList.isEmpty() -> View.VISIBLE
                else -> View.GONE
            }


            adapter.submitList(repositoryList)
            if(isNotifyDataSetChanged)
                adapter.notifyDataSetChanged()

        }
    }




    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        // Add menu items here
        menuInflater.inflate(R.menu.menu_sort, menu)
        if(U.getSharedPrefStringVal(requireContext(), C.SORT_BY)== C.BY_UPDATE) {
            menu.findItem(R.id.sort_by_last_update_id).isChecked = true
        } else if(U.getSharedPrefStringVal(requireContext(), C.SORT_BY)== C.BY_STAR) {
            menu.findItem(R.id.sort_by_star_id).isChecked = true
        }
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        // Handle the menu selection
        return when (menuItem.itemId) {
            R.id.sort_by_star_id -> {
                menuItem.isChecked = !menuItem.isChecked
                Log.d(TAG, "sort by star Clicked")
                U.setSharedPrefStringVal(requireContext(), C.SORT_BY, C.BY_STAR)
                getData(true)
                true
            }
            R.id.sort_by_last_update_id -> {
                menuItem.isChecked = !menuItem.isChecked
                Log.d(TAG, "Clicked")
                U.setSharedPrefStringVal(requireContext(), C.SORT_BY, C.BY_UPDATE)
                getData(true)
                true
            }
            else -> false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}